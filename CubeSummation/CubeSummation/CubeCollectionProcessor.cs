﻿using System;
using System.Text;

namespace CubeSummation
{
    /// <summary>
    /// Process a group of cubes; depends of the console input to works.
    /// </summary>
    public class CubeCollectionProcessor
    {
        public string Start()
        {
            StringBuilder str = new StringBuilder();
            int totalTestCases = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < totalTestCases; i++)
            {
                string cubeHeader = Console.ReadLine();
                int size = Convert.ToInt32(cubeHeader.Split(' ')[0]);
                int instructions = Convert.ToInt32(cubeHeader.Split(' ')[1]);
                CubeSummationApi.Processor.CubeSummation cube = new CubeSummationApi.Processor.CubeSummation(size, instructions);
                for (int j = 0; j < instructions; j++)
                {
                    cube.ProcessInstruction(Console.ReadLine());
                }
                str.Append(cube.GetCubeResult);
            }
            return str.ToString();
        }
    }
}
