﻿using System;

namespace CubeSummation
{
    class Program
    {
        static void Main(string[] args)
        {
            CubeCollectionProcessor processor = new CubeCollectionProcessor();
            Console.WriteLine(processor.Start());
        }
    }
}
