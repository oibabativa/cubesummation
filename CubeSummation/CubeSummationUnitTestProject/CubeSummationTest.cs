﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeSummationUnitTestProject
{
    [TestClass]
    public class CubeSummationTest
    {
        [TestMethod]
        public void TestMethodFirst()
        {
            /*Input:
                4 5
                UPDATE 2 2 2 4
                QUERY 1 1 1 3 3 3
                UPDATE 1 1 1 23
                QUERY 2 2 2 4 4 4
                QUERY 1 1 1 3 3 3
              Result:
                4
                4
                27
            */
            CubeSummationApi.Processor.CubeSummation cube = new CubeSummationApi.Processor.CubeSummation(4, 5);
            cube.ProcessInstruction("UPDATE 2 2 2 4");
            cube.ProcessInstruction("QUERY 1 1 1 3 3 3");
            cube.ProcessInstruction("UPDATE 1 1 1 23");
            cube.ProcessInstruction("QUERY 2 2 2 4 4 4");
            cube.ProcessInstruction("QUERY 1 1 1 3 3 3");
            string expectedResult = $"4{Environment.NewLine}4{Environment.NewLine}27{Environment.NewLine}";
            System.Diagnostics.Debug.WriteLine(expectedResult);
            System.Diagnostics.Debug.WriteLine(cube.GetCubeResult);
            Assert.AreEqual(expectedResult, cube.GetCubeResult);
        }

        [TestMethod]
        public void TestMethodSecond()
        {
            /*Input:
                2 4
                UPDATE 2 2 2 1
                QUERY 1 1 1 1 1 1
                QUERY 1 1 1 2 2 2
                QUERY 2 2 2 2 2 2
              Result:
                0
                1
                1
            */
            CubeSummationApi.Processor.CubeSummation cube = new CubeSummationApi.Processor.CubeSummation(2,4);
            cube.ProcessInstruction("UPDATE 2 2 2 1");
            cube.ProcessInstruction("QUERY 1 1 1 1 1 1");
            cube.ProcessInstruction("QUERY 1 1 1 2 2 2");
            cube.ProcessInstruction("QUERY 2 2 2 2 2 2");
            string expectedResult = $"0{Environment.NewLine}1{Environment.NewLine}1{Environment.NewLine}";
            System.Diagnostics.Debug.WriteLine(expectedResult);
            System.Diagnostics.Debug.WriteLine(cube.GetCubeResult);
            Assert.AreEqual(expectedResult, cube.GetCubeResult);
        }
    }
}
