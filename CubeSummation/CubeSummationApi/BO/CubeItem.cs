﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeSummationApi.BO
{
    /// <summary>
    /// Represents a cell in the matrix
    /// </summary>
    public class CubeItem
    {
        /// <summary>
        /// X position in the matrix
        /// </summary>
        public int XCoord { get; set; }
        /// <summary>
        /// Y position in the matrix
        /// </summary>
        public int YCoord { get; set; }
        /// <summary>
        /// Z position in the matrix
        /// </summary>
        public int ZCoord { get; set; }
        /// <summary>
        /// Cell's value
        /// </summary>
        public long Value { get; set; }

        public CubeItem(int XCoord, int YCoord, int ZCoord, long Value)
        {
            this.XCoord = XCoord;
            this.YCoord = YCoord;
            this.ZCoord = ZCoord;
            this.Value = Value;
        }
    }
}
