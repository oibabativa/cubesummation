﻿using CubeSummationApi.BO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeSummationApi.Processor
{
    /// <summary>
    /// Contains all logic to process a single cube
    /// </summary>
    public class CubeSummation
    {
        private List<CubeItem> cells = new List<CubeItem>();
        public int Size { get; private set; }
        public int Instructions { get; set; }
        private StringBuilder strBuildProcessResult;

        public CubeSummation(int size, int instructions)
        {
            this.Size = size;
            this.Instructions = instructions;
            this.strBuildProcessResult = new StringBuilder();
        }

        public void ProcessInstruction(string instructionLine)
        {
            string[] instruction = instructionLine.Split(' ');

            int[] splitHelper = instruction.Skip(1).Select<string, int>(int.Parse).ToArray();

            switch (instruction[0])
            {
                case "UPDATE":
                    Update(splitHelper[0], splitHelper[1], splitHelper[2], splitHelper[3]);
                    break;
                case "QUERY":
                    strBuildProcessResult.AppendLine(Query(splitHelper[0], splitHelper[1], splitHelper[2], splitHelper[3], splitHelper[4], splitHelper[5]).ToString());
                    break;
                default:
                    break;
            }
        }

        private void Update(int x1, int y1, int z1, long value)
        {
            var element = new CubeItem(x1, y1, z1, value);

            var prevItem = cells.Find(item => item.XCoord == x1 && item.YCoord == y1 && item.ZCoord == z1);

            if (cells.Exists(item => item.XCoord == x1 && item.YCoord == y1 && item.ZCoord == z1))
            {
                cells.RemoveAt(cells.IndexOf(prevItem));
            }
            cells.Add(element);
        }

        private long Query(int x1, int y1, int z1, int x2, int y2, int z2)
        {
            return cells.Sum(item =>
            {
                if (item.XCoord >= x1 && item.XCoord <= x2 &&
                item.YCoord >= y1 && item.YCoord <= y2 &&
                item.ZCoord >= z1 && item.ZCoord <= z2)
                    return item.Value;
                return 0;
            });
        }

        public string GetCubeResult
        {
            get { return strBuildProcessResult.ToString(); }
        }
    }

}
