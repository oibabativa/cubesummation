﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApp.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cube Summation</title>
    <meta charset="utf-8" />
    <!--START jQuery -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>

    <!--START BOOTSTRAP-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link href="css/styles.css" rel="stylesheet" />
</head>
<body>
    <!-- Begin page content -->
    <div class="container">
        <div class="page-header">
            <h1>Prueba t&eacute;cnica para Backend Grability</h1>
        </div>
        <p class="lead">
            Esta aplicaci&oacute;n proporciona una soluci&oacute;n para el problema
            <a href="https://www.hackerrank.com/challenges/cube-summation" target="_blank">Cube Summation</a>
        </p>
        <br />
        <form id="form1" runat="server">
            <br />
            <asp:TextBox ID="txtCase" class="form-control" runat="server" Rows="7" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea id="testCase" rows="7"></textarea>--%>
            <br />
            <asp:Button ID="btnExecute" class="btn btn-success" runat="server" Text="EJECUTAR" OnClick="btnExecute_Click" />
            <%--<button type="button" class="btn btn-success" onclick="index.main()">EJECUTAR</button>--%>
            <br />
            <h4>Resultado:</h4>
            <asp:TextBox ID="txtResult" class="form-control" runat="server" Rows="7" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea id="result" class="form-control" rows="5" disabled></textarea>--%>
            <br />
        </form>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted">Autor Omar Babativa - 2017</p>
        </div>
    </footer>
</body>
</html>
