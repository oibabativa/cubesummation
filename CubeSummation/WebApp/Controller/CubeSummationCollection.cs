﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Controller
{
    public class CubeSummationCollection
    {
        internal string ProcessFullTest(string text)
        {
            string[] fullInput = text.Split('\n');
            int lineCounter = 0;

            StringBuilder str = new StringBuilder();
            int totalTestCases = Convert.ToInt32(fullInput[lineCounter++]);
            for (int i = 0; i < totalTestCases; i++)
            {
                string cubeHeader = fullInput[lineCounter++];
                int size = Convert.ToInt32(cubeHeader.Split(' ')[0]);
                int instructions = Convert.ToInt32(cubeHeader.Split(' ')[1]);
                CubeSummationApi.Processor.CubeSummation cube = new CubeSummationApi.Processor.CubeSummation(size, instructions);
                for (int j = 0; j < instructions; j++)
                {
                    cube.ProcessInstruction(fullInput[lineCounter++]);
                }
                str.Append(cube.GetCubeResult);
            }
            return str.ToString();

            
        }
    }
}
